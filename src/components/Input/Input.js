import styles from "./Input.module.scss";
import PropTypes from "prop-types";
import React from "react";


const Input = ({tag:Tag, name,label, maxLength})=>(
    <div className={styles.formItem}>
        <Tag
            className={Tag === 'input'? styles.input : styles.textarea}
            type="text"
            name={name}
            id={name}
            placeholder=" "
            maxLength={maxLength}
            required
        />
        <label className={styles.label}
               htmlFor={name}>{label}
        </label>
        <div className={styles.formItemBar}></div>
    </div>
);
Input.propTypes ={
    tag: PropTypes.string,
    name: PropTypes.string.isRequird,
    label: PropTypes.string.isRequird,
    maxLength: PropTypes.number,
}
Input.defaultProps ={
    tag: 'input',
    maxLength: 200
}

export default Input;
