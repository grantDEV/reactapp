import React from 'react';
import styles from "./Title.module.scss";


const Title = ({name}) => (
    <h2 className={styles.name}>
        {name}
    </h2>

)
export default Title;
