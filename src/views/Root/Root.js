import React from 'react';
import './index.css';
import {BrowserRouter, Route} from "react-router-dom";
import TwitterView from "../TwietterView/TwiterView";
import ArticleViews from "../ArticleView/ArticleView";
import NotesView from "../NotesView/NotesView";

const initialStateItems = [
    {
        image: 'http://www.helsinkitimes.fi/images/2018/Aug/helsinki-2005916_1280.jpg',
        name: 'Dan Abramov',
        description: 'Working on @reactjs. The demo guy.',
        twitterLink: 'https://twitter.com/dan_abramov',
    },
    {
        image: 'http://www.helsinkitimes.fi/images/2018/Aug/helsinki-2005916_1280.jpg',
        name: 'Ryan Florence',
        description: 'Making React accessible for users and developers at https://reach.tech . Online learning, workshops, OSS, and consulting.',
        twitterLink: 'https://twitter.com/ryanflorence',
    },
    {
        image: 'http://www.helsinkitimes.fi/images/2018/Aug/helsinki-2005916_1280.jpg',
        name: 'Michael Jackson',
        description: 'Maker. Co-author of React Router. Working on @ReactTraining. Created @unpkg. Head over heels for @cari.',
        twitterLink: 'https://twitter.com/mjackson',
    },
    {
        image: 'http://www.helsinkitimes.fi/images/2018/Aug/helsinki-2005916_1280.jpg',
        name: 'Kent C. Dodds',
        description: 'Making software development more accessible · Husband, Father, Latter-day Saint, Teacher, OSS, GDE, @TC39 · @PayPalEng @eggheadio @FrontendMasters · #JS',
        twitterLink: 'https://twitter.com/kentcdodds',
    },
]
class Root extends React.Component{
    state ={
        items:[...initialStateItems],
    }
    addItem = (e) =>{
        e.preventDefault();

        const newItem ={
            name: e.target[0].value,
            twitterLink: e.target[1].value,
            image: e.target[2].value,
            description: e.target[3].value,
        }

        this.setState(prevSate =>({
           items: [...prevSate.items, newItem]
        }));

        e.target.reset();
    }

    render() {
        return(
            <BrowserRouter>
                <>
                    <h1>Heloo</h1>
                    <Route exact path="/" component={TwitterView}/>
                    <Route path="/article" component={ArticleViews}/>
                    <Route path="/notes" component={NotesView}/>
                </>
            </BrowserRouter>
        )
    }
}

export default Root;
